const express = require('express')
const path = require('path')
const app = express()
const mongo = require('mongodb').MongoClient;
const session = require('express-session')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
const passport = require('passport');
const auth = require('./src/auth/auth');
const fs = require('fs');
const http = require('http');
const https = require('https');
const cors = require('cors');
const ObjectId = require('mongodb').ObjectID;
const multer = require('multer');
const mongoose = require('mongoose');
const formidable = require('formidable');
const cloudinary = require('cloudinary')

require('dotenv').config()
const StudentGradesDatabase = require('./src/student-grades-database.js');

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_KEY,
  api_secret: process.env.CLOUD_SECRET
});

const mongo_url = process.env.MONGO_URL + process.env.DB_NAME;
const db_name = process.env.DB_NAME;
const port = process.env.PORT;
const session_secret = process.env.SECRET;
const frontend_url = process.env.FRONTEND_URL;

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './files/');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});
var upload = multer({ storage : storage}).single('file');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
var corsOptions = {
    origin: '*',
    credentials: true };
app.use(cors(corsOptions));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "https://onlearn.coderkalyan.com");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

auth(passport);
app.use(passport.initialize());

app.use(cookieSession({
    name: 'session',
    keys: ['jfeiaojfe;oaij'],
    maxAge: 240000
}));
app.use(cookieParser());

require('./src/lessonsclasses.js')(app, mongo, mongoose);

/**
 * Checks if User is logged in
 */
app.post('/api/v1/islogged', (req, res) => {
  if (req.session.token) {
      res.cookie('token', req.session.token);
      return res.json({ result : true});
  } else {
      res.cookie('token', '')
      return res.json({ result : false});
  }
});

/**
 * Logs the user out.
 */
app.get('/api/v1/logout', (req, res) => {
  req.logout();
  req.session = null;
  return res.json({ result : true });
});

async function checkUser(req, res) {
  var User = require('./src/schemas/user.js');
  await mongoose.connect(process.env.MONGO_URL, {
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASSWORD,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  User.findOne({ "id" : req.user.profile.id}, function(err, user) {
    if (err) throw err;
    if (user == null) {
      var doc = new User({
        id: req.user.profile.id,
        FullName:  req.user.profile.displayname,
        FirstName:  req.user.profile.name.givenName,
        Picture: req.user.profile._json.picture,
        Classes: [],
      });

      doc.save(function(err) {
        if (err) throw err;
        console.log("NEW USER CREATED")
      });
    }
    else {
      console.log("USER FOUND")
    }
  });
}


/**
 * Logs the user in with Google
 */
app.get('/api/v1/auth/google', passport.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/userinfo.profile']
}));

app.get('/api/v1/auth/google/callback',
    passport.authenticate('google', {
        failureRedirect: frontend_url + '/welcome'
    }),
    (req, res) => {
        req.session.token = req.user.token;
        checkUser(req,res);
        res.cookie('token', req.session.token);
        res.redirect(frontend_url);
    }
);

app.post('/api/v1/studentgrades', (req, res, body) => {
  res.header('Access-Control-Allow-Origin', "*");
  res.send(StudentGradesDatabase.StudentGradesDatabase(req.body.student));
});

app.get('/getungradedsubmissions', (req, res) => {
  res.header('Access-Control-Allow-Origin', "*");
  res.send({
    ungradedSubmissions: StudentGradesDatabase.getUngradedSubmissions()
  });
});

app.post('/submitassignment', (req, res, body) =>{
  res.header('Access-Control-Allow-Origin', "*");
  StudentGradesDatabase.addSubmission(req.body.student, req.body.assignment, req.body.submission)
  res.send({
    submitted: true
  });
});

app.post('/gradeASubmission', (req, res, body) =>{
  res.header('Access-Control-Allow-Origin', "*");
  StudentGradesDatabase.gradeSubmission(req.body)
  res.send({
    done: true
  })
})


app.post('/api/v1/files/new', async function(req,res){
  //if (req.session) {
    new formidable.IncomingForm().parse(req)
      .on('file', (name, file) => {
        cloudinary.v2.uploader.upload(file.path,{folder: "onlearn", resource_type: "auto"},async function(error, result) {
            console.log("RESULT" + result);
            return res.json({url : result});
        });
      })
      .on('aborted', () => {
        console.error('Request aborted by the user')
      })
      .on('error', (err) => {
        console.error('Error', err)
        throw err
      })
      /*
      .on('end', function() {
        res.end();
      });*/
  //}
  //else {
  //  return res.json({ result : false})
  //}

})

app.listen(port || 3001);

/*
var privateKey = fs.readFileSync( 'private.key.pem' );
var certificate = fs.readFileSync( 'domain.cert.pem' );

https.createServer({
    key: privateKey,
    cert: certificate
}, app).listen(port);

http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80);
*/
