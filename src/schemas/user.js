// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
const userSchema = new Schema({
  id : String,
  FullName:  String,
  FirstName: String,
  Picture: String,
  Classes: [String]
});

var userSch = mongoose.model('User', userSchema);

module.exports = userSch;
