// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
const lessonSchema = new Schema({
  Title:  String,
  Class: String,
  Elements: {}
});
var lessonSch = mongoose.model('Lesson', lessonSchema);

module.exports = lessonSch;
