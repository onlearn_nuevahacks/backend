// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
const classSchema = new Schema({
  Name:  String,
  Teacher: String,
  Join: String,
  Members: [String],
  Lessons: [{
    Title: String,
    Id:  Number
  }]
});

var classsch = mongoose.model('Classe', classSchema);

module.exports = classsch;
