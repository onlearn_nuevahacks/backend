var Class = require('./schemas/class.js');
var User = require('./schemas/user.js');
var Lesson = require('./schemas/lesson.js');

const check = require('./auth/check')
const ObjectId = require('mongodb').ObjectID;
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?');


module.exports = function(app, mongo, mongoose) {


  app.get('/api/v1/users/profile', check, async function(req, res) {
    User.findOne({ "id" : req.user.profile.id}, function(err, user) {
      if (user == null) {
        return res.json({ result : false });
      }
      return res.send(user);
    });
  });


  app.post('/api/v1/classes/new',  check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    var classID = shortid.generate()
    var doc = new Class({
      Name:  req.body.name,
      Teacher: req.session.user,
      Join: classID,
      Members: [req.session.user],
    });

    doc.save(function(err) {
      if (err) throw err;
      User.findOne({ "id" : req.user.profile.id}, function(err, user) {
        if (user == null) {
          return res.json({ result : false });
        }
        user.Classes.push(doc._id);
        user.save(function(err) {
          if (err) throw err;
          return res.json({ result : true });
        });
      });
    });
  });


  app.post('/api/v1/classes/delete', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    Class.findOne({ "_id" : ObjectId(req.body.classid)}, function(err, user) {
      if (user == null) {
        return res.json({result : false})
      }
      if (user.Teacher == req.session.user) {
        Class.deleteOne({ "_id" : ObjectId(req.body.classid)}, function(err) {
          if (err) {
            console.log(err);
            return res.json({result : false})
          }
          return res.json({result: true})
        });
      }
      else {
        return res.json({result : false})
      }
    });
  });

  app.post('/api/v1/lessons/delete', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    Lessons.findOne({ "_id" : ObjectId(req.body.classid)}, function(err, user) {
      if (user == null) {
        return res.json({result : false})
      }
      if (user.Teacher == req.session.user) {
        Lessons.deleteOne({ "_id" : ObjectId(req.body.classid)}, function(err) {
          if (err) {
            console.log(err);
            return res.json({result : false})
          }
          return res.json({result: true})
        });
      }
      else {
        return res.json({result : false})
      }
    });
  });

  app.put('/api/v1/lessons/edit', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    Lesson.findOne({ "_id" : ObjectId(req.body.lessonid) }, function(err, answer) {
      Class.findOne({ "_id" : ObjectId(user.Class)}, function(err, checking) {
        if (checking.Teacher == req.session.user) {}
        else return res.json({ result : false })
      })
      if (err) throw err;
      if (user == null) {
        return res.json({ result : false})
      }
      answer.Title = req.body.Title;
      answer.Elements = req.body.Elements;

      answer.save(function(err) {
        if (err) throw err;
        return res.json({ result : true });
      });
    });

  })

  app.post('/api/v1/classes/join', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    Class.findOne({ "Join" : req.body.join}, function(err, user) {
      if (user == null) {
        return res.json({ result : false });
      }
      user.Members.push(req.user.profile.id);
      user.save(function(err) {
        if (err) throw err;
        return res.json({ result : true });
      });
    });
  });

  app.post('/api/v1/classes/id', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    Class.findOne({ "_id" : ObjectId(req.body.id) }, function(err, user) {
      if (err) throw err;
      if (user == null) {
        return res.json({ result : false})
      }
      return res.send(user)
    });
  });

  app.post('/api/v1/lessons/id', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    Lesson.findOne({ "_id" : ObjectId(req.body.id) }, function(err, user) {
      if (err) throw err;
      if (user == null) {
        return res.json({ result : false})
      }
      return res.send(user)
    });
  });

  app.post('/api/v1/lessons/new', check, async function(req, res) {
    await mongoose.connect(process.env.MONGO_URL, {
      user: process.env.MONGO_USER,
      pass: process.env.MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    var doc = new Lesson({
      Title:  req.body.title,
      Class: req.body.classid,
      Elements: [{}]
    });

    doc.save(function(err) {
      if (err) throw err;
      Class.findOne({ "_id" : ObjectId(req.body.classid)}, function(err, user) {
        if (user == null) {
          return res.json({ result : false });
        }
        user.Lessons.push(doc._id);
        user.save(function(err) {
          if (err) throw err;
          return res.json({ result : true });
        });
      });
    });
  });


}
