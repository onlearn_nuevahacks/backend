
module.exports = function(req, res, next) {
  if (req.session.token) {
      next();
  } else {
      res.cookie('token', '')
      return res.json({ result : false });
  }
}
