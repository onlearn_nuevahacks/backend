var allStudents = //this is just a placeholder for student grades
[
	{
		name: 'Student1',
		assignments: [
			{
				assignmentName: 'Finish the Hackathon',
				dateAssigned: 'Apr 10 2020',
				dateDue: 'Apr 17 2020',
				pointsPossible: 100,
				score: 83,
				extraCredit: false,
				notGraded: false,
				comments: null,
			},
			{
				assignmentName: 'Ungraded Homework',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 0,
				score: 0,
				extraCredit: false,
				notGraded: true,
				comments: 'this assignment was not graded',
			},
			{
				assignmentName: 'Extra Credit',
				dateAssigned: 'Mar 01 2020',
				dateDue: 'Mar 05 2020',
				pointsPossible: 10,
				score: 10,
				extraCredit: true,
				notGraded: false,
				comments: null,
			}
		]
	},
	{
		name: 'Student2',
		assignments: [
			{
				assignmentName: 'Finish the Hackathon',
				dateAssigned: 'Apr 10 2020',
				dateDue: 'Apr 17 2020',
				pointsPossible: 100,
				score: 32,
				extraCredit: false,
				notGraded: false,
				comments: 'good concept, poor execution',
			},
			{
				assignmentName: 'History Essay',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 17,
				score: 15,
				extraCredit: false,
				notGraded: false,
				comments: 'exemplary',
			},
			{
				assignmentName: 'Ungraded Homework',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 0,
				score: 0,
				extraCredit: false,
				notGraded: true,
				comments: 'this assignment was not graded',
			}
		]
	},
	{
		name: 'Student3',
		assignments: [
			{
				assignmentName: 'Finish the Hackathon',
				dateAssigned: 'Apr 10 2020',
				dateDue: 'Apr 17 2020',
				pointsPossible: 100,
				score: 99,
				extraCredit: false,
				notGraded: false,
				comments: 'Amazing!',
			},
			{
				assignmentName: 'History Essay',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 17,
				score: 5,
				extraCredit: false,
				notGraded: false,
				comments: 'foundational',
			},
			{
				assignmentName: 'Ungraded Homework',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 0,
				score: 0,
				extraCredit: false,
				notGraded: true,
				comments: 'this assignment was not graded',
			}
		]
	},
	{
		name: 'Student4',
		assignments: [
			{
				assignmentName: 'Finish the Hackathon',
				dateAssigned: 'Apr 10 2020',
				dateDue: 'Apr 17 2020',
				pointsPossible: 100,
				score: 75,
				extraCredit: false,
				notGraded: false,
				comments: null,
			},
			{
				assignmentName: 'Extra Credit',
				dateAssigned: 'Mar 01 2020',
				dateDue: 'Mar 05 2020',
				pointsPossible: 10,
				score: 10,
				extraCredit: true,
				notGraded: false,
				comments: null,
			},
			{
				assignmentName: 'Ungraded Homework',
				dateAssigned: 'Jan 01 2020',
				dateDue: 'May 05 2020',
				pointsPossible: 0,
				score: 0,
				extraCredit: false,
				notGraded: true,
				comments: 'this assignment was not graded',
			}
		]
	}	
];

var ungradedSubmissions = [];

module.exports = {
   getGrades: function(student) {
      let grades = [];
      for (let i = 0; i < allStudents.length; i++) {
      	if(allStudents[i].name === student){
      		grades = allStudents[i].assignments;
      	}
      }
      return(grades);
   },
   addSubmission: function(student, assignment, submission) {
   	ungradedSubmissions.push({
   		student: student,
   		assignment: assignment,
   		submission: submission
   	});
   },
   getUngradedSubmissions: function(){
   	return(ungradedSubmissions)
   },
   gradeSubmission: function(body){
   	for (var i = 0; i < allStudents.length; i++) {
   		if(allStudents[i].name === body.student.replace(/\s/g, '')){
   			var matchExistingAssignments = false;
   			for (var j = 0; j < allStudents[i].assignments[j].length; i++) {
   				if(allStudents[i].assignments.assignmentName === 'body.assignmentName'){
   					allStudents[i].assignments[j] = {
   						assignmentName: body.assignmentName,
   						dateAssigned: body.dateAssigned,
   						dateDue: body.dateDue,
   						pointsPossible: body.pointsPossible,
   						score: body.score,
   						extraCredit: body.extraCredit,
   						notGraded: body.notGraded,
   						comments: body.comments
   					}
   				}
   			}
   			if (!matchExistingAssignments){
   				allStudents[i].assignments.push({
   					assignmentName: body.assignmentName,
   					dateAssigned: body.dateAssigned,
   					dateDue: body.dateDue,
   					pointsPossible: body.pointsPossible,
   					score: body.score,
   					extraCredit: body.extraCredit,
   					notGraded: body.notGraded,
   					comments: body.comments
   				});
   			}
   		}
   		for (var k = 0; k < ungradedSubmissions.length; k++){
   			if (ungradedSubmissions[k].student.replace(/\s/g, '') === body.student.replace(/\s/g, '') && ungradedSubmissions[k].assignment.replace(/\s/g, '') === body.assignment.replace.replace(/\s/g, '')){
   				ungradedSubmissions.splice(k, 1);
   			}
   		}
   	}
   }
}